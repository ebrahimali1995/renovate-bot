


module.exports = {
  platform: 'gitlab',
  onboardingConfig: {
    extends: ['config:base'],
  },
  repositories: [
    "ebrahimali1995/minimal-reproduction"
  ],


  updateLockFiles: true,

  allowedPostUpgradeCommands: [
    "echo",
    "yarn install",
    "ls",
    "cd",
    "npx husky install"
  ],

  postUpgradeTasks: {
    // yarn install needed for java projects, see BR-3777, BR-4736
    "commands": [
      'echo "_auth=${NPM_TOKEN}" >> .npmrc',
      "ls -lisa",
      "yarn install --ignore-engines --ignore-platform --network-timeout 100000 --ignore-scripts && ls node_modules -lisa",
      "ls -lisa",
      "ls node_modules -lisa",
      "npx husky install"
    ],
  },
  }
